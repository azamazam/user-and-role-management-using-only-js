<!DOCTYPE html>

<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <script src="bootstrap/js/jquery.js" type="text/javascript"></script>
    <script src='bootstrap/js/bootstrap.min.js'></script>
    <script src='bootstrap/js/jquery.min.js'></script>

    <script src="SecurityManager.js"></script>
    <title>Assignment 1</title>

</head>
<body>
<header>
<h1 id='name'> Security Manager </h1>
    <nav id='main-nav' style="  border-top: 6px solid darkcyan; " class='navbar navbar-inverse navbar-responsive'>
        <div class="container-fluid">
            <ul class="nav navbar-nav ">
                <li class="active">
                    <a href="welcomeAdmin.php" class="">Home</a>
                </li>
                <li>
                    <a href="UserManagement.php"> User Management
                    </a>
                </li>
                <li>
                    <a href="roleManagement.php">Role Management
                    </a>

                </li>
                <li>
                    <a href="permissionManagement.php">Permission Management</a>
                </li>
                <li>
                    <a href="rolePermissionAssignment.php">Role-Permission Assignment</a>
                </li>
                <li>
                    <a href="userRoleAssignment.php">User-Role Assignment</a>
                </li>

            </ul>
            <ul class="navbar-right nav navbar-nav ">
                <li>
                    <a href="index.php">logout</a>
                </li>
            </ul>
        </div>
</nav>
</header>

</body>
</html>