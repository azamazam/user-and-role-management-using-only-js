<?php include('navbar.php');
/**
 * Created by PhpStorm.
 * User: DeskTop
 * Date: 3/6/2018
 * Time: 7:59 PM
 */
?>
<html>
<head>
    <script>
        function Main() {


            var userName = document.getElementById("userName");
            var name = document.getElementById("Name");
            var email = document.getElementById("email");
            var password = document.getElementById("password");
            var confirmPassword = document.getElementById("confirmPassword");
            var savebtn = document.getElementById("savebtn");
            var countrycmb = document.getElementById("cmbCountries");
            var citycmb = document.getElementById("cmbCities");
            var table = document.getElementById("userTable");



            var users = SecurityManager.GetAllUsers();
            var countries = SecurityManager.GetCountries();
            //     alert(JSON.stringify(users));


            var isUserNameVerify = true;
            var isNameVerify = true;
            var isEmailVerify = true;
            var isPasswordVerify = true;
            var isCountryVerify = true;
            var isCityVerify = true;

            userName.onfocusout = checkUserName;
            name.onfocusout = checkName;
            email.onfocusout = checkEmail;
            password.onfocusout = checkPassword;
            confirmPassword.onfocusout = matchPassword;
            citycmb.onchange = checkCity;

            var userData={};

            tablefill();

            function tablefill() {


                for (user in users) {
                    var row = document.createElement("tr");
                    table.appendChild(row);

                    var datainrow = document.createElement("td");
                    datainrow.innerText = users[user]["ID"];
                    row.appendChild(datainrow);

                    datainrow = document.createElement("td");
                    datainrow.innerText = users[user]["userName"];
                    row.appendChild(datainrow);

                    datainrow = document.createElement("td");
                    datainrow.innerText = users[user]["name"];
                    row.appendChild(datainrow);

                    datainrow = document.createElement("td");
                    datainrow.innerText = users[user]["email"];
                    row.appendChild(datainrow);

                    datainrow = document.createElement("td");
                    datainrow.innerHTML = "<a href='#' >edit</a>"
                    datainrow.setAttribute("id", users[user]["ID"]);
                    datainrow.setAttribute("onclick", "editUser(" + users[user]["ID"] + ");")
                    row.appendChild(datainrow);

                    datainrow = document.createElement("td");
                    datainrow.innerHTML = "<a href='#' >delete</a>"
                    datainrow.setAttribute("id", users[user]["ID"]);
                    datainrow.setAttribute("onclick", "deleteUser(" + users[user]["ID"] + ");")
                    row.appendChild(datainrow);

                }
            }

            editUser = function (userId) {
                userData = SecurityManager.GetUserById(userId);
                userName.value = userData["userName"];
                name.value = userData["name"];
                email.value = userData["email"];
                password.value = userData["password"];
                confirmPassword.value = password.value;


                countrycmb.innerHTML = ' <option>--select--</option>';
                for (var i = 0; i < countries.length; i++) {
                    var opt = document.createElement("option");
                    opt.setAttribute("value", countries[i].CountryID);
                    opt.innerText = countries[i].Name;
                    if (userData["country"] == countries[i].CountryID)
                        opt.setAttribute("selected", "selected");
                    countrycmb.appendChild(opt);
                }
                citycmb.innerHTML = ' <option>--select--</option>';

                var cities = SecurityManager.GetCitiesByCountryId(userData["country"]);
                //   alert(country.value);
                for (var i = 0; i < cities.length; i++) {
                    var opt = document.createElement("option");
                    opt.setAttribute("value", cities[i].CityID);
                    opt.innerText = cities[i].Name;
                    if (userData["city"] == cities[i].CityID)
                        opt.setAttribute("selected", "selected");
                    citycmb.appendChild(opt);
                }


            }
            deleteUser = function (userId) {
                var isdelete = confirm("Are You sure  You want  to delete User " + (SecurityManager.GetUserById(userId))["userName"]);
                if (isdelete)
                    SecurityManager.DeleteUser(userId, loadPage, err);

            }


            //fill  countryy  list
            for (var i = 0; i < countries.length; i++) {
                var opt = document.createElement("option");
                opt.setAttribute("value", countries[i].CountryID);
                opt.innerText = countries[i].Name;
                countrycmb.appendChild(opt);
            }
            countrycmb.onchange = function () {

                //Remove all child elements (e.g. options)
                citycmb.innerHTML = ' <option>--select--</option>';

                var cities = SecurityManager.GetCitiesByCountryId(countrycmb.value);
                //   alert(country.value);
                for (var i = 0; i < cities.length; i++) {
                    var opt = document.createElement("option");
                    opt.setAttribute("value", cities[i].CityID);
                    opt.innerText = cities[i].Name;
                    citycmb.appendChild(opt);
                }
                checkCountry();
            }


            function checkUserName() {
                if (userName.value.trim(' ') == 0) {
                    //       alert("please  Enter User Name");
                    userName.style.border = "1px solid red";
                    isUserNameVerify = false;
                }
                else if (userName.value.indexOf(' ') >= 0) {
                    //         alert("Enter User Name without spaces");
                    userName.style.border = "1px solid red";
                    isUserNameVerify = false;
                }
                else {
                    userName.style.border = "1px solid black";
                    isUserNameVerify = true;
                }
                var f = false;
                for (user in users) {
                    if (userName.value == users[user]["userName"]&& users[user]["ID"]!=userData["ID"])
                        f = true;
                }
                if (f) {
                    userName.style.border = "1px solid red";
                    isUserNameVerify = false;
                    alert("User Name already  exists");
                }
            }
            function checkName() {
                if (name.value.trim(' ') == 0) {
                    //            alert("please  Enter Name");
                    name.style.border = "1px solid red";
                    isNameVerify = false;
                }
                else {
                    name.style.border = "1px solid black";
                    isNameVerify = true;
                }
            }


            function validateEmail(emailvalue) {
                var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                return re.test(emailvalue);
            }

            function checkEmail() {
                var f = false;
                for (user in users) {
                    if (email.value == users[user]["email"] && users[user]["ID"] != userData["ID"]) {
                        f = true;
                        break;
                    }
                }
                if (!validateEmail(email.value) || f == true) {
                    //          alert("please  provide email");
                    email.style.border = "1px solid red";
                    isEmailVerify = false;
                }
                else {
                    email.style.border = "1px solid black";
                    isEmailVerify = true;
                }


            }


            function checkPassword() {
                if (password.value.length == 0) {
              //      alert("Please Enter password");
                    password.style.border = "1px solid red";
                    isPasswordVerify = false;
                }
                else {
                    password.style.border = "1px solid black";
                    isPasswordVerify = true;      //why  why error  if  remove this   ?
                }
            }


            function matchPassword() {

                if (password.value != confirmPassword.value || password.value.length == 0) {
                  //  alert("Both Passwords  does not  match");
                    password.style.border = "1px solid red";
                    confirmPassword.style.border = "1px solid red";
                    isPasswordVerify = false;
                    password.value = confirmPassword.value = '';
                }
                else {
                    password.style.border = "1px solid black";
                    confirmPassword.style.border = "1px solid black";
                    isPasswordVerify = true;
                }
            }


            function checkCountry() {

                if (countrycmb.value == "--select--") {
                    countrycmb.style.border = "1px solid red";
                    isCountryVerify = false;
                }
                else {
                    countrycmb.style.border = "1px solid black";
                    isCountryVerify = true;
                }
            }


            function checkCity() {

                if (citycmb.value == "--select--") {
                    citycmb.style.border = "1px solid red";
                    isCityVerify = false;
                }
                else {
                    citycmb.style.border = "1px solid black";
                    isCityVerify = true;
                }
            }


            savebtn.onclick = function () {
                checkUserName();
                checkName();
                checkEmail();
                checkPassword();
                matchPassword();
                checkCountry();
                checkCity();

                if (isUserNameVerify &&
                    isNameVerify &&
                    isEmailVerify &&
                    isCountryVerify &&
                    isCityVerify &&
                    isPasswordVerify) {
                    userData["userName"] = userName.value;
                    userData["name"] = name.value;
                    userData["email"] = email.value;
                    userData["password"] = password.value;
                    userData["country"] = countrycmb.value;
                    userData["city"] = citycmb.value;

                  //  alert(JSON.stringify(userData));
                    SecurityManager.SaveUser(userData, loadPage, err);
                    //  alert(userName.value + name.value + email.value + password.value + confirmPassword.value + country.value + city.value);
                } else alert("Some value are missing");

            }

            function loadPage() {
                window.location.href = "UserManagement.php";
            }

            function err() {
                alert("there is  some  problem ");
            }

        }

    </script>
</head>
<body onload="Main()">
<div class="container-fluid">
    <div class="row form-group col-lg-4 col-lg-offset-2">
        <form>
            <legend align="center">User Management</legend>
            <div class="form-group">
                <label>UserName:*</label>
                <input type="text" id="userName" class="form-control">
            </div>
            <div class="form-group"><label>Name:*</label>
                <input type="text" id="Name" class="form-control">
            </div>
            <div class="form-group"><label>Email:*</label>
                <input type="email" id="email" class="form-control">
            </div>
            <div class="form-group">
                <label>Password:*</label>
                <input type="password" id="password" class="form-control">
            </div>
            <label>Confirm Password:</label>
            <input type="password" id="confirmPassword" class="form-control">
            <div class="form-group"><label>Country:</label>
                <select id="cmbCountries" class="form-control">
                <option>--select--</option>
            </select>
            </div>
            <div class="form-group">
                <label>City</label>
                <select id="cmbCities" class="form-control">
                    <option>--select--</option>
                </select>
            </div>
            <div class="form-group">
                <input type="button" id="savebtn" value="Save" class="btn  btn-success col-lg-6">
                <input type="reset" value="Clear" class="btn  btn-warning col-lg-6">
            </div>
        </form>
    </div>
    <div class="col-lg-5 col-lg-offset-1">

        <table class="table" id="userTable">
            <legend align="center">Users Table</legend>
            <tr>
                <th>ID</th>
                <th>userName</th>
                <th>Name</th>
                <th>Email</th>
                <th>Edit</th>
                <th>Delete</th>
            </tr>
        </table>
    </div>
</div>
</body>
</html>