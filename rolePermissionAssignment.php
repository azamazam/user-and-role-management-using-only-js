<?php include("navbar.php");
/**
 * Created by PhpStorm.
 * Role: DeskTop
 * Date: 3/9/2018
 * Time: 10:51 PM
 */


?>
<html>
<head>
    <script>


        function Main() {
            //      alert("Roles Permission Management ");
            var savebtn = document.getElementById("savebtn");
            var roleNamecmb = document.getElementById("roleName");
            var permissionNamecmb = document.getElementById("permissionName");
            var rolePermissionTable = document.getElementById("rolePermissionTable");

            var roles = SecurityManager.GetAllRoles();
            var permissions = SecurityManager.GetAllPermissions();
            var rolesPermissions = SecurityManager.GetAllRolePermissions();

            var rolePermissionData = {};
            var isRoleNameVerify = true;
            var isPermissionNameVerify = true;
            var isRolePermissionVerify = true;

            roleNamecmb.onchange = checkRoleName;
            permissionNamecmb.onchange = checkPermissionName;
            savebtn.onclick = saveRolePermission;

            tablefill();
            fillcomboboxes();

            function tablefill() {

                for (rp in rolesPermissions) {
                    var row = document.createElement("tr");
                    rolePermissionTable.appendChild(row);

                    var datainrow = document.createElement("td");
                    datainrow.innerText = rolesPermissions[rp]["ID"];
                    row.appendChild(datainrow);

                    datainrow = document.createElement("td");
                    datainrow.innerText = rolesPermissions[rp]["roleName"];
                    row.appendChild(datainrow);

                    datainrow = document.createElement("td");
                    datainrow.innerText = rolesPermissions[rp]["permissionName"];
                    row.appendChild(datainrow);


                    datainrow = document.createElement("td");
                    datainrow.innerHTML = "<a href='#' >edit</a>"
                    datainrow.setAttribute("onclick", "editRolePermission(" + rolesPermissions[rp]["ID"] + ");")
                    row.appendChild(datainrow);

                    datainrow = document.createElement("td");
                    datainrow.innerHTML = "<a href='#' >delete</a>"
                    datainrow.setAttribute("onclick", "deleteRolePermission(" + rolesPermissions[rp]["ID"] + ");")
                    row.appendChild(datainrow);
                }
            }


            function fillcomboboxes() {
                for (role  in  roles) {
                    var op = document.createElement("option");
                    op.innerText = roles[role].roleName;
                    roleNamecmb.appendChild(op);
                }

                for (p in  permissions) {
                    var op = document.createElement("option");
                    op.innerText = permissions[p].permissionName;
                    permissionNamecmb.appendChild(op);
                }
            }


            function checkRoleName() {
                if (roleName.value == "--select--") {
                    //       alert("please  Enter role Name");
                    roleNamecmb.style.border = "1px solid red";
                    isRoleNameVerify = false;
                }
                else {
                    roleNamecmb.style.border = "1px solid black";
                    isRoleNameVerify = true;
                }

            }

            function checkPermissionName() {
                if (permissionNamecmb.value == "--select--") {
                    //       alert("please  Enter role Name");
                    permissionName.style.border = "1px solid red";
                    isPermissionNameVerify = false;
                }
                else {
                    permissionNamecmb.style.border = "1px solid black";
                    isPermissionNameVerify = true;
                }
            }

            function checkrolePermission() {
                isRolePermissionVerify = true;
                for (rp in rolesPermissions) {

                    if (rolesPermissions[rp].roleName == roleNamecmb.value &&
                        rolesPermissions[rp].permissionName == permissionNamecmb.value &&
                        rolesPermissions[rp].ID != rolePermissionData.ID) {

                        roleNamecmb.style.border = "1px solid red";
                        permissionNamecmb.style.border = "1px solid red";
                        isRolePermissionVerify = false;
                    }

                }
            }

            function saveRolePermission() {
                checkRoleName();
                checkPermissionName();
                checkrolePermission();
                if (isRoleNameVerify && isPermissionNameVerify && isRolePermissionVerify) {
                    rolePermissionData["roleName"] = roleNamecmb.value;
                    rolePermissionData["permissionName"] = permissionNamecmb.value;

              //      alert(JSON.stringify(rolePermissionData));
                    SecurityManager.SaveRolePermission(rolePermissionData, loadPage, err);

                } else alert("Some Thing went wrong \n Fileds Are empty or This Role has this permission already");

            }

            editRolePermission = function (roleId) {
                rolePermissionData = SecurityManager.GetRolePermissionById(roleId);
                roleNamecmb.value = rolePermissionData["roleName"];
                permissionNamecmb.value = rolePermissionData["permissionName"];

            }
            deleteRolePermission = function (roleId) {
                var isdelete = confirm("Are You sure  You want  to delete Role " + (SecurityManager.GetRolePermissionById(roleId))["roleName"]);
                if (isdelete)
                    SecurityManager.DeleteRolePermission(roleId, loadPage, err);
            }

            function loadPage() {
                window.location.href = "rolePermissionAssignment.php";
            }

            function err() {
                alert("there is  some  problem ");
            }

        }

    </script>
</head>
<body onload="Main();">
<div class="container-fluid">
    <div class="row form-group col-lg-4 col-lg-offset-2">
        <form>
            <legend align="center">Roles-Permission-Assignment</legend>
            <div class="form-group">
                <label>Role Name:*</label>
                <select id="roleName" class="form-control">
                    <option>--select--</option>
                </select>
            </div>
            <div class="form-group"><label>Permission:</label>
                <select id="permissionName" class="form-control">
                    <option>--select--</option>
                </select>
            </div>
            <div class="form-group">
                <input type="button" id="savebtn" value="Save" class="btn  btn-success col-lg-6">
                <input type="reset" value="Clear" class="btn  btn-warning col-lg-6">
            </div>
        </form>
    </div>
    <div class="col-lg-5 col-lg-offset-1">

        <table class="table" id="rolePermissionTable">
            <legend align="center">Role Permission Table</legend>
            <tr>
                <th>ID</th>
                <th>Role Name</th>
                <th>Permission</th>
                <th>Edit</th>
                <th>Delete</th>
            </tr>
        </table>
    </div>
</div>
</body>
</html>

