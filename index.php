<!DOCTYPE html>

<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <script src="bootstrap/js/jquery.js" type="text/javascript"></script>
    <script src='bootstrap/js/bootstrap.min.js'></script>
    <script src='bootstrap/js/jquery.min.js'></script>

    <script src="SecurityManager.js"></script>
    <title>Assignment 1</title>
    <script>
        function Main() {

            var adminloginbtn = document.getElementById('adminlogin');
            var userloginbtn = document.getElementById("userbtnlogin");


            adminloginbtn.onclick = function () {
                var adminUserName = document.getElementById("adminUser");
                var adminPassword = document.getElementById("adminPassword");
                if (adminUserName.value.trim(' ') == 0 && adminPassword.value.length == 0) {
                    alert("Please Enter User Name And Password ");

                    return;
                }
                if (adminUserName.value.trim(' ') == 0) {
                    alert("Please Enter User Name");
                    return;
                }
                if (adminPassword.value.length == 0) {
                    alert("Please Enter password");
                    return;
                }
                var result = SecurityManager.ValidateAdmin(adminUserName.value, adminPassword.value);
                if (result == true)
                    window.location.href = "welcomeAdmin.php";
                else {
                    alert("UserName or  Password  is  Incorrect ");
                    adminPassword.value = "";
                }
            }
            userloginbtn.onclick = function () {
                debugger;
                var UserName = document.getElementById("userName");
                var userPassword = document.getElementById("userPassword");
                if (UserName.value.trim(' ') == 0 && userPassword.value.length == 0) {
                    alert("Please Enter User Name And Password ");
                    return;
                }
                if (UserName.value.trim(' ') == 0) {
                    alert("Please Enter User Name");
                    return;
                }
                if (userPassword.value.length == 0) {
                    alert("Please Enter password");
                    return;
                }
                var users = SecurityManager.GetAllUsers();
               var  result=false;
                var currentuser;
                for (i in users){
                    if (users[i].userName==UserName.value &&users[i].password==userPassword.value){
                        result=true;
                        currentuser = users[i];
                    }
                }
                if (result == true) {
                    window.location.href = "userpage.php";
                    localStorage["userid"] = currentuser.ID;
                }
                else {
                    alert("UserName or  Password  is  Incorrect ");
                    userPassword.value = "";
                }

            }
        }
    </script>

</head>

<body onload="Main();">
<h1>Security Manager </h1><hr>


    <form class="form col-lg-4 col-lg-offset-1">
        <legend>login Admin</legend>
        <div class="form-group">
        <label>UserName :</label>
        <input id="adminUser" type="text" class="form-control">
        </div>
        <div class="form-group">
        <label>Password :</label>
        <input id="adminPassword" type="password" class="form-control">

        </div>
        <div class="form-group">

            <input type="button" value="login" id="adminlogin" class=" btn btn-block btn-success"/>
        </div>

    </form>

    <form class="form col-lg-4 col-lg-offset-1">
        <legend>login User</legend>
        <div class="form-group">
            <label class="control-label">UserName :</label>
            <input id="userName" type="text" class="form-control">
        </div>
        <div class="form-group">
            <label>Password :</label>
            <input id="userPassword" type="password" class="form-control">
        </div>
        <div class="form-group">
            <input type="button" value="login" id="userbtnlogin" class="btn btn-block btn-success">
        </div>

    </form>
</body>

</html>