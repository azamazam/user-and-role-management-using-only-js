<?php include("navbar.php");
/**
 * Created by PhpStorm.
 * Role: DeskTop
 * Date: 3/9/2018
 * Time: 10:51 PM
 */
?>
<html>
<head>
    <script>
        function Main() {
            //      alert("Roles Mangement ");
            var savebtn = document.getElementById("savebtn");
            var roleName = document.getElementById("roleName");
            var description = document.getElementById("description");
            var roleTable = document.getElementById("roleTable");

            var roles = SecurityManager.GetAllRoles();

            var isRoleNameVerify = true;
            var roleData = {};

            roleName.onfocusout = checkRoleName;
            savebtn.onclick = saveRole;

            tablefill();

            function tablefill() {

                for (role in roles) {
                    var row = document.createElement("tr");
                    roleTable.appendChild(row);

                    var datainrow = document.createElement("td");
                    datainrow.innerText = roles[role]["ID"];
                    row.appendChild(datainrow);

                    datainrow = document.createElement("td");
                    datainrow.innerText = roles[role]["roleName"];
                    row.appendChild(datainrow);

                    datainrow = document.createElement("td");
                    datainrow.innerText = roles[role]["description"];
                    row.appendChild(datainrow);


                    datainrow = document.createElement("td");
                    datainrow.innerHTML = "<a href='#' >edit</a>"
                    datainrow.setAttribute("id", roles[role]["ID"]);
                    datainrow.setAttribute("onclick", "editRole(" + roles[role]["ID"] + ");")
                    row.appendChild(datainrow);

                    datainrow = document.createElement("td");
                    datainrow.innerHTML = "<a href='#' >delete</a>"
                    datainrow.setAttribute("id", roles[role]["ID"]);
                    datainrow.setAttribute("onclick", "deleteRole(" + roles[role]["ID"] + ");")
                    row.appendChild(datainrow);

                }
            }


            function checkRoleName() {
                if (roleName.value.trim() == 0) {
                    //       alert("please  Enter role Name");
                    roleName.style.border = "1px solid red";
                    isRoleNameVerify = false;
                }
                else {
                    roleName.style.border = "1px solid black";
                    isRoleNameVerify = true;
                }
                var f = false;
                for (role in roles) {
                    if (roleName.value == roles[role]["roleName"] && roles[role]["ID"] != roleData["ID"]) {
                        f = true;
                        break;
                    }
                }
                if (f) {
                    roleName.style.border = "1px solid red";
                    isRoleNameVerify = false;
                    //   alert("Role  already  exists");
                }

            }

            function saveRole() {
                checkRoleName();


                if (isRoleNameVerify) {
                    roleData["roleName"] = roleName.value;
                    roleData["description"] = description.value;

                    //    alert(JSON.stringify(roleData));
                    SecurityManager.SaveRole(roleData, loadPage, err);

                } else alert("Some value are missing");

            }

            editRole = function (roleId) {
                roleData = SecurityManager.GetRoleById(roleId);
                roleName.value = roleData["roleName"];
                description.value = roleData["description"];

            }
            deleteRole = function (roleId) {
                var isdelete = confirm("Are You sure  You want  to delete Role " + (SecurityManager.GetRoleById(roleId))["roleName"]);
                if (isdelete)
                    SecurityManager.DeleteRole(roleId, loadPage, err);
            }

            function loadPage() {
                window.location.href = "roleManagement.php";
            }

            function err() {
                alert("there is  some  problem ");
            }

        }


    </script>
</head>
<body onload="Main();">
<div class="container-fluid">
    <div class="row form-group col-lg-4 col-lg-offset-2">
        <form>
            <legend align="center">Roles</legend>
            <div class="form-group">
                <label>Role Name:*</label>
                <input type="text" id="roleName" class="form-control">
            </div>
            <div class="form-group"><label>Description:</label>
                <input type="text" id="description" class="form-control">
            </div>
            <div class="form-group">
                <input type="button" id="savebtn" value="Save" class="btn  btn-success col-lg-6">
                <input type="reset" value="Clear" class="btn  btn-warning col-lg-6">
            </div>
        </form>
    </div>
    <div class="col-lg-5 col-lg-offset-1">

        <table class="table" id="roleTable">
            <legend align="center">Role Table</legend>
            <tr>
                <th>ID</th>
                <th>Role Name</th>
                <th>Description</th>
                <th>Edit</th>
                <th>Delete</th>
            </tr>
        </table>
    </div>
</div>
</body>
</html>

